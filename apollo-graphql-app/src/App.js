import * as React from 'react';
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';

import { ToolHeader } from './components/ToolHeader';
import { CarTable } from './components/CarTable';
import { CarForm } from './components/CarForm';

const APP_QUERY = gql`
  query AppQuery {
    headerText @client
    editCarId @client
    cars {
      id
      make
      model
      year
      color
      price
    }
  }
`;

const APPEND_CAR_MUTATION = gql`
  mutation AppendCar($car: AppendCar) {
    setEditCarId(carId: -1) @client
    appendCar(car: $car) {
      id
      make
      model
      year
      color
      price
    }
  }
`;

const REPLACE_CAR_MUTATION = gql`
  mutation ReplaceCar($car: ReplaceCar) {
    setEditCarId(carId: -1) @client
    replaceCar(car: $car) {
      id
      make
      model
      year
      color
      price
    }
  }
`;

const DELETE_CAR_MUTATION = gql`
  mutation DeleteCar($carId: ID!) {
    setEditCarId(carId: -1) @client
    deleteCar(carId: $carId) {
      id
      make
      model
      year
      color
      price
    }
  }
`;

const SET_EDIT_CAR_ID_MUTATION = gql`
  mutation SetEditCarId($carId: ID!) {
    setEditCarId(carId: $carId) @client
  }
`;

export class App extends React.Component {

  render() {

    return <>

      <Mutation mutation={REPLACE_CAR_MUTATION}>
        {(mutateReplaceCar) => {

          const replaceCar = car => {
            mutateReplaceCar({
              variables: { car },
              refetchQueries: [ { query: APP_QUERY } ],
            });
          };

          return <Mutation mutation={DELETE_CAR_MUTATION}>
            {(mutateDeleteCar) => {

              const deleteCar = carId => {
                mutateDeleteCar({
                  variables: { carId },
                  refetchQueries: [ { query: APP_QUERY } ],
                });
              };

              return <Mutation mutation={SET_EDIT_CAR_ID_MUTATION}>
                {(mutateSetEditCarId) => {
        
                  const editCar = carId => {
                    mutateSetEditCarId({
                      variables: { carId }
                    });
                  };
        
                  return <Query query={APP_QUERY}>
                    {({ data, loading, error }) => {
            
                      if (loading) {
                        return <div>Loading...</div>;
                      }
            
                      if (error) {
                        return <div>Error: {error.message}</div>
                      }
            
                      return <>
                        <ToolHeader headerText={data.headerText} />
                        <CarTable cars={data.cars} editCarId={data.editCarId}
                          onEditCar={editCar} onDeleteCar={deleteCar}
                          onSaveCar={replaceCar} onCancelCar={() => editCar(-1)} />
                      </>;
                    }}
                  </Query>;
                }}
              </Mutation>;
              
            }}
          </Mutation>;
  

        }}
      </Mutation>

      


      
      <Mutation mutation={APPEND_CAR_MUTATION}>
        {(mutate) => {

          const appendCar = car => {

            mutate({
              variables: { car },
              refetchQueries: [
                { query: APP_QUERY },
              ],
              // optimisticResponse: {
              //   appendCar: {
              //     id: -1,
              //     ...car,
              //     __typename: 'Car',
              //   },
              // },
              // update: (store, { data: { appendCar: car } }) => {
              //   console.log(car);
              //   const data = store.readQuery({ query: APP_QUERY });
              //   data.cars = data.cars.concat(car);
              //   store.writeQuery({ query: APP_QUERY, data })
              // },
            });

          };

          return <CarForm buttonText="Add Car" onSubmitCar={appendCar} />;
        }}
      </Mutation>
    </>;

  }
}
