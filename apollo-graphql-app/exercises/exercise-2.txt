Exercise 2

1. Copy the CarTable component (and any extra components needed) to the Apollo application. You may put them in the components folder (create the folder if needed)

2. Utilize the CarTable component in App.js to display the table of cars. Do not worry about the button on the row, don't delete them but just provide noop functions to them.

3. Ensure it works.


