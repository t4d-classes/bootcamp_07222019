import fetch from 'node-fetch';

export const resolvers = {
  Query: {
    message: () => 'Hello World!',
    cars: (_1, _2, { restURL }) => {

      return fetch(`${restURL}/cars`)
        .then(res => res.json());
    },
  },
  Mutation: {
    appendCar: (_, { car }, { restURL }) => {
      return fetch(`${restURL}/cars`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(car),
      })
        .then(res => res.json());
    },
    replaceCar: (_, { car }, { restURL }) => {

      return fetch(`${restURL}/cars/${encodeURIComponent(car.id)}`)
        .then(res => res.json())
        .then(originalCar => {

          return fetch(`${restURL}/cars/${encodeURIComponent(car.id)}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(car),
          })
            .then(() => originalCar);

        });
    },
    deleteCar: (_, { carId }, { restURL }) => {

      return fetch(`${restURL}/cars/${encodeURIComponent(carId)}`)
        .then(res => res.json())
        .then(originalCar => {

          return fetch(`${restURL}/cars/${encodeURIComponent(carId)}`, {
            method: 'DELETE',
          })
            .then(() => originalCar);

        });
    },
  },
};
