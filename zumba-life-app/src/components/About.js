import React from 'react';
import { Route, Link } from 'react-router-dom';

export const About = () => {

  return <>
    <h1>About</h1>

    <ul>
      <li><Link to="/about/team">Team</Link></li>
      <li><Link to="/about/location">Location</Link></li>
      <li><Link to="/about/music">Music</Link></li>
    </ul>    

    <Route path="/about/team" render={() => {

      return <>
        <h2>Team ZumZum</h2>
        <p>
          We are the best Zumba players in the world.
        </p>
      </>;

    }} />

    <Route path="/about/location" render={() => {

      return <>
        <h2>Located in Mountain View</h2>
        <p>
        We Zumba in the best place on earth...
        </p>
      </>;

    }} />

    <Route path="/about/music" render={() => {

      return <>
        <h2>Despacito</h2>
        <p>
          We crush our enemies while we Zumba to Despacito!
        </p>
      </>;

    }} />

  </>;
};