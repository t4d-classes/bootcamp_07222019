import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';

import { Home } from './components/Home';
import { About } from './components/About';
import { Contact } from './components/Contact';


ReactDOM.render(
  <Router>
    <>
      <h1>Full Contact Zumba</h1>

      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/about">About</Link></li>
        <li><Link to="/contact/1">Contact</Link></li>
      </ul>

      <div>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact/:id" component={Contact} />
      </div>
    </>
  </Router>,
  document.getElementById('root'),
);

