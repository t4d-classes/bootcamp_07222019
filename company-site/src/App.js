import React from 'react';
import logo from './logo.svg';

import './App.css';

function App() {
  return (
    <div className="container">
      <header>
        <h1>Company Web Site</h1>
      </header>
      <nav>
        <ul className="menuItems">
          <li className="menuItem"><a href="#">Home</a></li>
          <li className="menuItem"><a href="#">About</a></li>
          <li className="menuItem"><a href="#">Contact</a></li>
        </ul>
      </nav>
      <aside>
        Sidebar
      </aside>
      <div id="content">
        Here is some content....
      </div>
      <footer>
        <small>&copy; {new Date().getFullYear()} A Cool Company, Inc.</small>
      </footer>
    </div>
  );
}

export default App;
